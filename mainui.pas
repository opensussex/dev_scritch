unit MainUi;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, db, dbf, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls, SynEdit, DBGrids, DBCtrls, LazFileUtils, StrUtils;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Dbf1: TDbf;
    ListBox1: TListBox;
    SynEdit1: TSynEdit;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure SynEdit1Change(Sender: TObject);
  private

  public

  end;

var
  Form1: TForm1;
  HomeDirectory: string;
  DbFileName: string;
  DbFileWithPath: string;
  SelectedSnippet: integer;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.FormCreate(Sender: TObject);
begin
     DbFileName := 'dev_scritch.dbf';
     HomeDirectory := GetUserDir();
     DbFileWithPath := HomeDirectory + DbFileName;
     Dbf1 := TDbf.Create(Self);
     Dbf1.TableName := DbFileWithPath;
     if not FileExistsUTF8(DbFileWithPath) then with Dbf1 do begin
       TableLevel:=7;
       Exclusive:=True;
       FieldDefs.Add('ID', ftAutoInc, 0, True);
       FieldDefs.Add('Title', ftString, 25, True);
       FieldDefs.Add('Content',ftMemo,0, True);
       CreateTable;
     end;

     Dbf1.Open;
     Dbf1.first;
     while not Dbf1.EOF do begin
       ListBox1.Items.Add(Dbf1.FieldByName('ID').AsString + ':' + Dbf1.FieldByName('Title').AsString);
       Dbf1.next;
     end;
     Dbf1.Close;

end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
     Dbf1.PackTable;
     Dbf1.Close;
end;

procedure TForm1.ListBox1Click(Sender: TObject);
var
   RecordId: string;
   ListItemSelected: string;
   delim : TSysCharSet;

begin
     delim:= [':'];
     ListItemSelected:= ListBox1.GetSelectedText;
     RecordId:= ExtractDelimited(1,ListItemSelected, delim);
     SelectedSnippet:=StrToInt(RecordId);
     with Dbf1 do begin
       Open;
       Locate('ID',SelectedSnippet,[]);
       SynEdit1.Text := FieldByName('Content').AsString;
       Close;
     end;
end;

procedure TForm1.SynEdit1Change(Sender: TObject);
begin
     with Dbf1 do begin
       Open;
       Locate('ID',SelectedSnippet,[]);
       Edit;
       FieldByName('Content').AsString := SynEdit1.Text;
       Post;
       Close;
     end;

end;

procedure TForm1.Button1Click(Sender: TObject);
var
  SnippetName: string;
  LastId: string;
begin
       SnippetName:= InputBox ('New Snippet',
                          'Enter name of new snippet',
                          '');

     with Dbf1 do begin
       Open;
       Append;
       Fields[1].AsString:=SnippetName;
       Post;
       Dbf1.Last;
       LastId:= Dbf1.FieldByName('ID').AsString;
       SelectedSnippet:=StrToInt(LastId)
     end;
     ListBox1.Items.Add(LastId + ':' + SnippetName);
     SynEdit1.Clear;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
     with Dbf1 do begin
       Open;
       Locate('ID',SelectedSnippet,[]);
       Delete;
       Close;
     end;
     ListBox1.DeleteSelected;
     SynEdit1.Text := ' ';
end;

end.

